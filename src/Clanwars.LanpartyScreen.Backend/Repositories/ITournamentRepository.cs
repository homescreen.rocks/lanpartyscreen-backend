using System.Collections.Generic;
using System.Threading.Tasks;
using Clanwars.LanpartyScreen.Backend.Entities;

namespace Clanwars.LanpartyScreen.Backend.Repositories;

public interface ITournamentRepository : IRandomlyAccessible<TournamentEntry>
{
    Task<IList<TournamentEntry>> AllAsync();
    Task<IList<TournamentEntry>> LastAsync(uint limitLatest);
    Task<bool> DoesItemExistAsync(uint tournamentId);
    Task<TournamentEntry?> FindAsync(uint tournamentId);
    Task InsertAsync(TournamentEntry tournament);
    Task UpdateAsync(TournamentEntry tournament);
    Task DeleteAsync(TournamentEntry tournament);
}
