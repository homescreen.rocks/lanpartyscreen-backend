using System.Collections.Generic;
using System.Threading.Tasks;
using Clanwars.LanpartyScreen.Backend.Entities;
using Clanwars.LanpartyScreen.Backend.Services.Pipeline;

namespace Clanwars.LanpartyScreen.Backend.Repositories;

public interface IPipelineRepository
{
    Task<bool> IsModuleActivatedAsync(ModuleType type);
    Task<IList<PipelineModuleState>> ActivationStatesAsync();
    Task EnableModuleAsync(ModuleType type);
    Task DisableModuleAsync(ModuleType type);
    Task<PipelineModuleState> SetModuleTimeoutsAsync(ModuleType type, uint timeoutInitial, uint timeoutRecurring);
    Task<(uint Initial, uint Recurring)> GetTimeoutForModuleAsync(ModuleType module);
}