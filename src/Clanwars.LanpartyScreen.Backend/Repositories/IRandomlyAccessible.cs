using System.Threading.Tasks;
using Clanwars.LanpartyScreen.Backend.Helpers.Exceptions;

namespace Clanwars.LanpartyScreen.Backend.Repositories;

public interface IRandomlyAccessible<TRepositoryEntryType>
{
    /// <summary>
    /// Get a random entry of repository.
    /// </summary>
    /// <exception cref="NoEntryInModuleException">Exception is thrown if not a single entry exists in this repository
    /// and thus can not randomly produce content.</exception>
    /// <returns>A random entry from repository.</returns>
    Task<TRepositoryEntryType> GetRandomAsync();
}