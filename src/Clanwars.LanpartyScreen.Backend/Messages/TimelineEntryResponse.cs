using System;

namespace Clanwars.LanpartyScreen.Backend.Messages;

public record TimelineEntryResponse
(
    uint Id,
    DateTime StartTime,
    string Duration,
    string Title,
    string? AdditionalInformation
);