using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Clanwars.LanpartyScreen.Backend.Entities;

namespace Clanwars.LanpartyScreen.Backend.Messages;

public record UpdateTournamentEntry
(
    [Required] string Name,
    [Required] int PlayersPerTeam,
    [Required] int Teams,
    [Required] int TeamLimit,
    [Required] TournamentMode Mode,
    [Required] TournamentState State,
    [Required] DateTime StartTime
);
