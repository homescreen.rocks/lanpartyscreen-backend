using System;
using System.ComponentModel.DataAnnotations;

namespace Clanwars.LanpartyScreen.Backend.Messages;

public record CreateShoutEntry
(
    [Required] string Name,
    [Required] string Text,
    DateTime? PostedAt
);