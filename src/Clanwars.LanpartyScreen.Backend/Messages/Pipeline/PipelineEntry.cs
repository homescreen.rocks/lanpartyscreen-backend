using Clanwars.LanpartyScreen.Backend.Services.Pipeline;

namespace Clanwars.LanpartyScreen.Backend.Messages.Pipeline;

public class PipelineEntry
{
    public ModuleType Type { get; set; }
    public bool IsNew { get; set; }
}

public class PipelineEntry<TPayload> : PipelineEntry
{
    [System.Text.Json.Serialization.JsonIgnore]
    public TPayload Payload = default!;
        
    [System.Text.Json.Serialization.JsonPropertyName("payload")]
    public object? SerializedTPayload => Payload;
}