using System.ComponentModel.DataAnnotations;

namespace Clanwars.LanpartyScreen.Backend.Messages;

public record CreateAnnouncementEntry(
    [Required] string Headline,
    [Required] string Text,
    [Required] string IconClass
);