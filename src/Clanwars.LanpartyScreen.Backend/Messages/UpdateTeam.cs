namespace Clanwars.LanpartyScreen.Backend.Messages;

public record UpdateTeam
(
    string Name,
    int Points
);
