using System;
using Clanwars.LanpartyScreen.Backend.Entities;
using Mapster;

namespace Clanwars.LanpartyScreen.Backend.Messages.Mappings;

public class MessagesMapsterRegister : IRegister
{
    public void Register(TypeAdapterConfig config)
    {
        config.NewConfig<PictureEntry, PictureEntryResponse>()
            .Map(response => response.Url, entry => "/api/v1/Pictures/" + entry.Id);

        config.NewConfig<CreateAnnouncementEntry, AnnouncementEntry>()
            .Map(dst => dst.PostedAt, src => DateTime.Now)
            .Ignore(dst => dst.Id)
            .Ignore(dst => dst.UpdatedAt!);

        config.NewConfig<UpdateAnnouncementEntry, AnnouncementEntry>()
            .Map(dst => dst.UpdatedAt, src => DateTime.Now)
            .Ignore(dst => dst.Id)
            .Ignore(dst => dst.PostedAt);

        config.NewConfig<AnnouncementEntry, AnnouncementEntryResponse>();


        config.NewConfig<CreateNewsEntry, NewsEntry>()
            .Map(dst => dst.PublishedAt, src => src.PublishedAt ?? DateTime.Now)
            .Ignore(dst => dst.Id);

        config.NewConfig<UpdateNewsEntry, NewsEntry>()
            .Ignore(dst => dst.Id);

        config.NewConfig<NewsEntry, NewsEntryResponse>();

        config.NewConfig<CreateShoutEntry, ShoutEntry>()
            .Ignore(dst => dst.Id)
            .Map(dst => dst.PostedAt, src => src.PostedAt ?? DateTime.Now);

        config.NewConfig<UpdateShoutEntry, ShoutEntry>()
            .Ignore(dst => dst.Id)
            .Map(dst => dst.PostedAt, src => src.PostedAt ?? DateTime.Now);

        config.NewConfig<ShoutEntry, ShoutEntryResponse>();

        config.NewConfig<CreateTournamentEntry, TournamentEntry>()
            .Ignore(dst => dst.Id)
            .Ignore(dst => dst.Matches);

        config.NewConfig<UpdateTournamentEntry, TournamentEntry>()
            .Ignore(dst => dst.Id)
            .Ignore(dst => dst.Matches);

        config.NewConfig<TournamentEntry, TournamentEntryResponse>();

        config.NewConfig<Match, MatchResponse>();

        config.NewConfig<Team, TeamResponse>();

        config.NewConfig<UpdateTeam, Team>()
            .Ignore(dst => dst.Id!);

        config.NewConfig<CreateWelcomeMessageEntry, WelcomeMessageEntry>()
            .Ignore(dst => dst.Id);
    }
}
