using Clanwars.LanpartyScreen.Backend.Entities;

namespace Clanwars.LanpartyScreen.Backend.Messages;

public record TournamentMatchResult
(
    Match Match,
    TournamentEntryResponse Tournament
);