using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Clanwars.LanpartyScreen.Backend.Entities;
using Clanwars.LanpartyScreen.Backend.Messages;
using Clanwars.LanpartyScreen.Backend.Repositories;
using Clanwars.LanpartyScreen.Backend.Services.Pipeline;
using Mapster;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Clanwars.LanpartyScreen.Backend.Controllers;

[Route("api/v1/[controller]")]
[ApiController]
public class TournamentsController : ControllerBase
{
    private readonly ITournamentRepository _tournamentRepository;
    private readonly IPipelinePublisher _publisher;

    public TournamentsController(ITournamentRepository tournamentRepository, IPipelinePublisher publisher)
    {
        _tournamentRepository = tournamentRepository;
        _publisher = publisher;
    }

    // GET api/v1/tournaments
    [HttpGet]
    [ProducesResponseType(StatusCodes.Status200OK)]
    public async Task<ActionResult<IList<TournamentEntryResponse>>> Get([FromQuery] uint? limit)
    {
        var result = limit is null
            ? await _tournamentRepository.AllAsync()
            : await _tournamentRepository.LastAsync(limit.Value);

        return Ok(result.Adapt<IList<TournamentEntryResponse>>());
    }

    // GET api/v1/tournaments/{id}
    [HttpGet("{tournamentId:int}")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<ActionResult<TournamentEntryResponse>> GetTournament([FromRoute] uint tournamentId)
    {
        var tournament = await _tournamentRepository.FindAsync(tournamentId);

        if (tournament is null)
        {
            return NotFound();
        }

        return Ok(tournament.Adapt<TournamentEntryResponse>());
    }

    // POST api/v1/tournaments
    [HttpPost]
    [ProducesResponseType(StatusCodes.Status201Created)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    public async Task<ActionResult<TournamentEntryResponse>> CreateTournament(
        [FromBody] CreateTournamentEntry createTournament)
    {
        var tournament = createTournament.Adapt<TournamentEntry>();
        await _tournamentRepository.InsertAsync(tournament);
        await _publisher.AddPipelineEntryAsync(await _tournamentRepository.AllAsync());

        return CreatedAtAction("GetTournament", new {tournamentId = tournament.Id}, tournament.Adapt<TournamentEntryResponse>());
    }

    // PUT api/v1/tournaments/{tournamentId}
    [HttpPut("{tournamentId:int}")]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<ActionResult> UpdateTournament(
        [FromRoute] uint tournamentId,
        [FromBody] UpdateTournamentEntry updateTournamentEntry
    )
    {
        var oldTournament = await _tournamentRepository.FindAsync(tournamentId);
        if (oldTournament == null)
        {
            return NotFound();
        }

        updateTournamentEntry.Adapt(oldTournament);

        await _tournamentRepository.UpdateAsync(oldTournament);
        await _publisher.AddPipelineEntryAsync(await _tournamentRepository.AllAsync());

        return NoContent();
    }

    // DELETE api/v1/tournaments/{tournamentId}
    [HttpDelete("{tournamentId:int}")]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<ActionResult> Delete([FromRoute] uint tournamentId)
    {
        var tournament = await _tournamentRepository.FindAsync(tournamentId);

        if (tournament is null)
        {
            return NotFound();
        }

        await _tournamentRepository.DeleteAsync(tournament);

        return NoContent();
    }

    // GET api/v1/tournaments/{id}/matches
    [HttpGet("{tournamentId:int}/Matches")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<ActionResult<IList<MatchResponse>>> GetMatches([FromRoute] uint tournamentId)
    {
        var tournament = await _tournamentRepository.FindAsync(tournamentId);
        if (tournament is null)
        {
            return NotFound();
        }

        return Ok(tournament.Matches.Adapt<List<MatchResponse>>());
    }

    // POST api/v1/tournaments/{tournamentId}/matches
    [HttpPost("{tournamentId:int}/Matches")]
    [ProducesResponseType(StatusCodes.Status201Created)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<ActionResult<MatchResponse>> AddMatch(
        [FromRoute] uint tournamentId,
        [FromBody] [Required] List<Team> teams)
    {
        var tournament = await _tournamentRepository.FindAsync(tournamentId);
        if (tournament is null)
        {
            return NotFound();
        }

        var match = new Match
        {
            Teams = teams
        };

        tournament.Matches.Add(match);
        await _tournamentRepository.UpdateAsync(tournament);

        return CreatedAtAction("GetMatch", new {tournamentId = tournament.Id, matchId = match.Id}, match.Adapt<MatchResponse>());
    }

    // GET api/v1/tournaments/{id}/matches/{matchId}
    [HttpGet("{tournamentId:int}/Matches/{matchId:int}")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<ActionResult<MatchResponse>> GetMatch(
        [FromRoute] uint tournamentId,
        [FromRoute] uint matchId
    )
    {
        var tournament = await _tournamentRepository.FindAsync(tournamentId);
        if (tournament is null)
        {
            return NotFound();
        }

        var match = tournament.Matches.FirstOrDefault(m => m.Id == matchId);
        if (match is null)
        {
            return NotFound();
        }

        return Ok(match.Adapt<MatchResponse>());
    }

    // PUT api/v1/tournaments/{tournamentId}/matches/{matchId}
    [HttpPut("{tournamentId:int}/Matches/{matchId:int}")]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<ActionResult> UpdateMatch(
        [FromRoute] uint tournamentId,
        [FromRoute] uint matchId,
        [FromBody] [Required] List<UpdateTeam> teams
    )
    {
        var tournament = await _tournamentRepository.FindAsync(tournamentId);
        if (tournament is null)
        {
            return NotFound();
        }

        var dbMatch = tournament.Matches.FirstOrDefault(m => m.Id == matchId);
        if (dbMatch is null)
        {
            return NotFound();
        }

        if (dbMatch.Teams.Count == 0)
        {
            dbMatch.Teams.AddRange(teams.Adapt<List<Team>>());
        }
        else
        {
            foreach (var dbMatchTeam in dbMatch.Teams)
            {
                foreach (var updateTeam in teams)
                {
                    if (dbMatchTeam.Name == updateTeam.Name)
                    {
                        dbMatchTeam.Points = updateTeam.Points;
                    }
                }
            }
        }

        await _tournamentRepository.UpdateAsync(tournament);

        if (dbMatch.Teams.Any(t => t.Points > 0))
        {
            await _publisher.AddPipelineEntryAsync(new TournamentMatchResult(dbMatch,
                tournament.Adapt<TournamentEntryResponse>())
            );
        }

        return NoContent();
    }

    // DELETE api/v1/tournaments/{tournamentId}/matches/{matchId}
    [HttpDelete("{tournamentId:int}/Matches/{matchId:int}")]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<ActionResult> DeleteMatch(
        [FromRoute] uint tournamentId,
        [FromRoute] uint matchId
    )
    {
        var tournament = await _tournamentRepository.FindAsync(tournamentId);
        if (tournament is null)
        {
            return NotFound();
        }

        var match = tournament.Matches.FirstOrDefault(m => m.Id == matchId);
        if (match is null)
        {
            return NotFound();
        }

        tournament.Matches.Remove(match);
        await _tournamentRepository.UpdateAsync(tournament);

        return NoContent();
    }
}
