namespace Clanwars.LanpartyScreen.Backend.Controllers;

public enum PipelineStatusAction
{
    Run,
    Stop,
    Skip
}