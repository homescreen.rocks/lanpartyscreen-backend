namespace Clanwars.LanpartyScreen.Backend.Controllers;

public enum PipelineModuleStatusAction
{
    Enable,
    Disable
}