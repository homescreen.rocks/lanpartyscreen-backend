using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Clanwars.LanpartyScreen.Backend.Entities;

[Table("Shouts")]
public class ShoutEntry
{
    [Key]
    public uint Id { get; set; }

    [Required]
    public string Name { get; set; } = null!;

    [Required]
    public string Text { get; set; } = null!;

    [Required]
    public DateTime PostedAt { get; set; }
}