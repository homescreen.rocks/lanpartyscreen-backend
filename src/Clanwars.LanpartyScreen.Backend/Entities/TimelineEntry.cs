using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Clanwars.LanpartyScreen.Backend.Entities;

[Table("TimelineEntries")]
public class TimelineEntry
{
    [Key]
    public uint Id { get; set; }

    [Required]
    public DateTime StartTime { get; set; }

    public string? Duration { get; set; } = null!;

    [Required]
    public string Title { get; set; } = null!;

    public string? AdditionalInformation { get; set; } = null!;
}