using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Clanwars.LanpartyScreen.Backend.Entities;

[Table("Pictures")]
public class PictureEntry
{
    [Key]
    public uint Id { get; set; }

    [Required]
    public string Image { get; set; } = null!;

    [Required]
    public string Title { get; set; } = null!;

    [Required]
    public bool ShowOnStream { get; set; }
}