using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Clanwars.LanpartyScreen.Backend.Entities;

[Table("Announcements")]
public class AnnouncementEntry
{
    [Key]
    public uint Id { get; set; }

    [Required]
    public string Headline { get; set; } = null!;

    [Required]
    public string Text { get; set; } = null!;

    [Required]
    public string IconClass { get; set; } = null!;

    [Required]
    public DateTime PostedAt { get; set; }
        
    public DateTime? UpdatedAt { get; set; }
}