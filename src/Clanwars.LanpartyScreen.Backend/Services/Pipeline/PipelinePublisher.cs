using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Timers;
using Clanwars.LanpartyScreen.Backend.Entities;
using Clanwars.LanpartyScreen.Backend.Helpers.Exceptions;
using Clanwars.LanpartyScreen.Backend.Hubs;
using Clanwars.LanpartyScreen.Backend.Messages;
using Clanwars.LanpartyScreen.Backend.Messages.Pipeline;
using Clanwars.LanpartyScreen.Backend.Repositories;
using Mapster;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace Clanwars.LanpartyScreen.Backend.Services.Pipeline;

public class PipelinePublisher : IPipelinePublisher
{
    private bool _autoPublishOn;
    private Queue<PipelineEntry> _queue = new();

    private readonly Timer _nextPublishTimer;
    private readonly ILogger _logger;
    private readonly IHubContext<PipelineHub> _pipelineHub;
    private readonly IHubContext<LightEventsHub> _lightEventsHub;
    private readonly Timer _reactivationTimer;

    public PipelinePublisher(
        ILogger<PipelinePublisher> logger,
        IHubContext<PipelineHub> pipelineHub,
        IHubContext<LightEventsHub> lightEventsHub,
        IServiceProvider services
    )
    {
        _logger = logger;

        _nextPublishTimer = new Timer(10 * 1000); // default tick is 10 seconds
        _nextPublishTimer.Elapsed += async (sender, e) => { await PublishNextAsync(sender, e); };
        _nextPublishTimer.AutoReset = false;

        _reactivationTimer = new Timer();
        _reactivationTimer.Elapsed += (sender, e) => { PublisherAutoMode = true; };
        _reactivationTimer.AutoReset = false;

        _pipelineHub = pipelineHub;
        _lightEventsHub = lightEventsHub;
        Services = services;
    }

    public IServiceProvider Services { get; }

    public bool PublisherAutoMode
    {
        get => _autoPublishOn;
        set
        {
            _autoPublishOn = value;
            switch (_autoPublishOn)
            {
                case true:
                    _pipelineHub.Clients.All.SendAsync("autoplay", "on");
                    // publish next entry immediately
                    PublishNextAsync(null, null).Wait();
                    break;
                case false:
                    _nextPublishTimer.Stop();
                    _pipelineHub.Clients.All.SendAsync("autoplay", "off");
                    break;
            }
        }
    }

    public PipelineEntry CurrentEntry { get; private set; }

    public void PublishStaticContent(PictureEntry picture, int timeoutSeconds = 600)
    {
        var next = new PipelineEntry<PictureEntryResponse>()
        {
            IsNew = false,
            Type = ModuleType.Pictures,
            Payload = picture.Adapt<PictureEntryResponse>()
        };
        PublishStaticContent(next, timeoutSeconds);
    }

    public void PublishStaticContent(AnnouncementEntry entry, int timeoutSeconds = 600)
    {
        var next = new PipelineEntry<AnnouncementEntryResponse>()
        {
            IsNew = false,
            Type = ModuleType.Announcements,
            Payload = entry.Adapt<AnnouncementEntryResponse>()
        };
        PublishStaticContent(next, timeoutSeconds);
    }

    private void PublishStaticContent(PipelineEntry next, int timeoutSeconds)
    {
        _reactivationTimer.Stop();
        if (PublisherAutoMode)
        {
            PublisherAutoMode = false;
        }

        CurrentEntry = next;
        _pipelineHub.Clients.All.SendAsync("publish", next);

        if (timeoutSeconds <= 0)
        {
            return;
        }

        _reactivationTimer.Interval = timeoutSeconds * 1000;
        _reactivationTimer.Start();
    }

    public async Task Skip()
    {
        if (!PublisherAutoMode)
        {
            return;
        }

        _nextPublishTimer.Stop();
        await PublishNextAsync(null, null);
    }

    private async Task PublishNextAsync(object? sender, ElapsedEventArgs? e)
    {
        var next = NextEntry();
        if (next == null)
        {
            _logger.LogInformation("Use some random content");
            try
            {
                next = await GetRandomEntry();
            }
            catch (StackOverflowException)
            {
                PublisherAutoMode = false;
                _logger.LogWarning("No content found! Stopped autoplay");
                return;
            }
        }
        else
        {
            _logger.LogInformation("Got Entry to publish from prioritized queue");
        }

        CurrentEntry = next;
        await _pipelineHub.Clients.All.SendAsync("publish", next);

        if (next.IsNew)
        {
            switch (next.Type)
            {
                case ModuleType.Shouts:
                    await _lightEventsHub.Clients.All.SendAsync("beam", "shout");
                    break;
                case ModuleType.MatchResult:
                    await _lightEventsHub.Clients.All.SendAsync("beam", "match-result");
                    break;
                case ModuleType.Announcements:
                    await _lightEventsHub.Clients.All.SendAsync("beam", "announcement");
                    break;
                case ModuleType.News:
                    await _lightEventsHub.Clients.All.SendAsync("beam", "news");
                    break;
                case ModuleType.WelcomeMessage:
                    await _lightEventsHub.Clients.All.SendAsync("beam", "welcome");
                    break;
            }
        }

        using (var scope = Services.CreateScope())
        {
            var pipelineRepository = scope.ServiceProvider.GetRequiredService<IPipelineRepository>();
            var (initial, recurring) = await pipelineRepository.GetTimeoutForModuleAsync(next.Type);
            _nextPublishTimer.Interval = (next.IsNew ? initial : recurring) * 1000;
        }

        _nextPublishTimer.Start();
    }

    private async Task<PipelineEntry> GetRandomEntry(int count = 0)
    {
        var next = new PipelineEntry();
        try
        {
            var random = new Random();
            var rand = random.Next(0, 140);

            using var scope = Services.CreateScope();
            if (rand >= 120)
            {
                //pictures
                var pipelineRepository = scope.ServiceProvider.GetRequiredService<IPipelineRepository>();
                if (!await pipelineRepository.IsModuleActivatedAsync(ModuleType.Pictures))
                {
                    throw new ModuleDisabledException();
                }

                _logger.LogInformation("Random content decision: Picture");
                var randPic = await scope.ServiceProvider.GetRequiredService<IPicturesRepository>().GetRandomAsync();

                var response = randPic.Adapt<PictureEntryResponse>();

                next = new PipelineEntry<PictureEntryResponse>()
                {
                    IsNew = false,
                    Type = ModuleType.Pictures,
                    Payload = response
                };
            }
            else if (rand >= 100)
            {
                //timeline
                var pipelineRepository = scope.ServiceProvider.GetRequiredService<IPipelineRepository>();
                if (!await pipelineRepository.IsModuleActivatedAsync(ModuleType.Timeline))
                {
                    throw new ModuleDisabledException();
                }

                _logger.LogInformation("Random content decision: Timeline");
                next = new PipelineEntry<Dictionary<DateTime, List<TimelineEntryResponse>>>()
                {
                    IsNew = false,
                    Type = ModuleType.Timeline,
                    Payload = (await scope.ServiceProvider.GetRequiredService<ITimelineRepository>().FullTimelineAsync()).Adapt<Dictionary<DateTime, List<TimelineEntryResponse>>>()
                };
            }
            else if (rand >= 80)
            {
                //news
                var pipelineRepository = scope.ServiceProvider.GetRequiredService<IPipelineRepository>();
                if (!await pipelineRepository.IsModuleActivatedAsync(ModuleType.News))
                {
                    throw new ModuleDisabledException();
                }

                _logger.LogInformation("Random content decision: News");
                next = new PipelineEntry<NewsEntryResponse>()
                {
                    IsNew = false,
                    Type = ModuleType.News,
                    Payload = (await scope.ServiceProvider.GetRequiredService<INewsRepository>().GetRandomAsync()).Adapt<NewsEntryResponse>()
                };
            }
            else if (rand >= 60)
            {
                // tournament overview
                var pipelineRepository = scope.ServiceProvider.GetRequiredService<IPipelineRepository>();
                if (!await pipelineRepository.IsModuleActivatedAsync(ModuleType.Tournaments))
                {
                    throw new ModuleDisabledException();
                }

                _logger.LogInformation("Random content decision: Tournament Overview");
                next = new PipelineEntry<IList<TournamentEntryResponse>>()
                {
                    IsNew = false,
                    Type = ModuleType.Tournaments,
                    Payload = (await scope.ServiceProvider.GetRequiredService<ITournamentRepository>().AllAsync()).Adapt<IList<TournamentEntryResponse>>()
                };
            }
            else if (rand >= 50)
            {
                // tournament upcoming matches
                var pipelineRepository = scope.ServiceProvider.GetRequiredService<IPipelineRepository>();
                if (!await pipelineRepository.IsModuleActivatedAsync(ModuleType.Matches))
                {
                    throw new ModuleDisabledException();
                }

                _logger.LogInformation("Random content decision: Tournament upcoming matches");
                next = new PipelineEntry<TournamentEntryResponse>()
                {
                    IsNew = false,
                    Type = ModuleType.Matches,
                    Payload = (await scope.ServiceProvider.GetRequiredService<ITournamentRepository>().GetRandomAsync()).Adapt<TournamentEntryResponse>()
                };
            }
            else if (rand >= 40)
            {
                // tournament finished matches
                var pipelineRepository = scope.ServiceProvider.GetRequiredService<IPipelineRepository>();
                if (!await pipelineRepository.IsModuleActivatedAsync(ModuleType.MatchResults))
                {
                    throw new ModuleDisabledException();
                }

                _logger.LogInformation("Random content decision: Tournament finished matches");
                next = new PipelineEntry<TournamentEntryResponse>()
                {
                    IsNew = false,
                    Type = ModuleType.MatchResults,
                    Payload = (await scope.ServiceProvider.GetRequiredService<ITournamentRepository>().GetRandomAsync()).Adapt<TournamentEntryResponse>()
                };
            }
            else if (rand >= 20)
            {
                // announcement
                var pipelineRepository = scope.ServiceProvider.GetRequiredService<IPipelineRepository>();
                if (!await pipelineRepository.IsModuleActivatedAsync(ModuleType.Announcements))
                {
                    throw new ModuleDisabledException();
                }


                _logger.LogInformation("Random content decision: Latest Announcement");
                next = new PipelineEntry<AnnouncementEntryResponse>()
                {
                    IsNew = false,
                    Type = ModuleType.Announcements,
                    Payload = (await scope.ServiceProvider.GetRequiredService<IAnnouncementRepository>().LatestAsync()).Adapt<AnnouncementEntryResponse>()
                };
            }
            else if (rand >= 0)
            {
                //shouts
                var pipelineRepository = scope.ServiceProvider.GetRequiredService<IPipelineRepository>();
                if (!await pipelineRepository.IsModuleActivatedAsync(ModuleType.Shouts))
                {
                    throw new ModuleDisabledException();
                }


                _logger.LogInformation("Random content decision: Shout");
                next = new PipelineEntry<ShoutEntryResponse>()
                {
                    IsNew = false,
                    Type = ModuleType.Shouts,
                    Payload = (await scope.ServiceProvider.GetRequiredService<IShoutRepository>().GetRandomAsync()).Adapt<ShoutEntryResponse>()
                };
            }
        }
        catch (Exception e) when (
            e is NoEntryInModuleException or ModuleDisabledException
        )
        {
            if (count > 70)
            {
                throw new StackOverflowException();
            }

            return await GetRandomEntry(++count);
        }

        return next;
    }

    public async Task<int> AddPipelineEntryAsync(NewsEntry entry)
    {
        var e = new PipelineEntry<NewsEntry>()
        {
            IsNew = true,
            Type = ModuleType.News,
            Payload = entry
        };
        return await AddPipelineEntryAsync(e);
    }

    public async Task<int> AddPipelineEntryAsync(ShoutEntry entry)
    {
        var e = new PipelineEntry<ShoutEntry>()
        {
            IsNew = true,
            Type = ModuleType.Shouts,
            Payload = entry
        };

        return await AddPipelineEntryAsync(e);
    }

    public async Task<int> AddPipelineEntryAsync(WelcomeMessageEntry entry)
    {
        var e = new PipelineEntry<WelcomeMessageEntry>()
        {
            IsNew = true,
            Type = ModuleType.WelcomeMessage,
            Payload = entry
        };

        return await AddPipelineEntryAsync(e);
    }

    public async Task<int> AddPipelineEntryAsync(IList<TournamentEntry> tournaments)
    {
        var e = new PipelineEntry<IList<TournamentEntry>>()
        {
            IsNew = true,
            Type = ModuleType.Tournaments,
            Payload = tournaments
        };

        return await AddPipelineEntryAsync(e);
    }

    public async Task<int> AddPipelineEntryAsync(Match match)
    {
        var e = new PipelineEntry<Match>()
        {
            IsNew = true,
            Type = ModuleType.MatchResult,
            Payload = match
        };

        return await AddPipelineEntryAsync(e);
    }

    public async Task<int> AddPipelineEntryAsync(TournamentMatchResult matchResult)
    {
        var e = new PipelineEntry<TournamentMatchResult>()
        {
            IsNew = true,
            Type = ModuleType.MatchResult,
            Payload = matchResult
        };

        return await AddPipelineEntryAsync(e);
    }

    private async Task<int> AddPipelineEntryAsync(PipelineEntry entry)
    {
        using (var scope = Services.CreateScope())
        {
            var pipelineRepository = scope.ServiceProvider.GetRequiredService<IPipelineRepository>();
            if (!await pipelineRepository.IsModuleActivatedAsync(entry.Type))
            {
                return _queue.Count;
            }
        }

        _queue.Enqueue(entry);
        return _queue.Count;
    }

    /// <summary>
    /// Loads next entry from the prioritized queue if any is queued.
    /// </summary>
    /// <returns>PipelineEntry or null if no entry is queued at the moment.</returns>
    public PipelineEntry? NextEntry()
    {
        return _queue.Count > 0 ? _queue.Dequeue() : null;
    }
}