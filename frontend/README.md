# Lanpartyscreen Frontends
This project mainly consists of two UIs:
* Beamer UI
* Control UI

## Development

Use the yarn jobs to serve developments servers for beamer/control:
* `yarn run beamer:start`
* `yarn run control:start`
