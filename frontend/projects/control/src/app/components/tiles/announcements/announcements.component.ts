import {Component, OnInit} from '@angular/core';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {AnnouncementsService} from '../../../services/announcements.service';
import {Announcement} from '../../../classes/announcement';
import {FormControl, FormGroup} from '@angular/forms';
import {faPencilAlt} from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-announcements',
  templateUrl: './announcements.component.html',
  styleUrls: ['./announcements.component.scss']
})
export class AnnouncementsComponent implements OnInit {
  faPencilAlt = faPencilAlt;

  announcements: Array<Announcement> = [];
  announcementForm = new FormGroup({
    id: new FormControl(),
    headline: new FormControl(''),
    text: new FormControl(''),
    iconClass: new FormControl(''),
    currentTimeout: new FormControl(),
    postedAt: new FormControl()
  });
  uploadingMessage;
  editMode = false;

  constructor(private service: AnnouncementsService, private modalService: NgbModal) {
    this.announcementForm.get('currentTimeout').setValue(10);
  }

  ngOnInit() {
    this.service.getAllAnnouncements().subscribe(announcements => this.announcements = announcements);
  }

  public openAddModal(modal) {
    this.announcementForm.reset();
    this.modalService.open(modal, {size: 'lg'}).result.then(() => {
      this.service.getAllAnnouncements().subscribe(announcements => this.announcements = announcements);
      this.editMode = false;
    }, () => {
      this.editMode = false;
    });
  }

  public openModal(modal, announcement: Announcement) {
    this.announcementForm.patchValue(announcement);
    this.announcementForm.get('currentTimeout').setValue(10);
    this.modalService.open(modal, {size: 'lg'}).result.then(() => {
      this.service.getAllAnnouncements().subscribe(announcements => this.announcements = announcements);
      this.editMode = false;
    }, () => {
      this.editMode = false;
    });
  }

  deleteAnnouncement(id: number, modal) {
    this.service.deleteAnnouncement(id).subscribe(() => modal.close('closed after deletion'));
  }

  saveAnnouncement(modal) {
    this.uploadingMessage = 'Uploading.... please wait...';
    this.service.addAnnouncement(this.announcementForm.value).subscribe(() => {
      this.uploadingMessage = null;
      this.announcementForm.reset();
      modal.close('closed after upload');
    });
  }

  updateAnnouncement(modal) {
    console.log('updating announcement');
    this.uploadingMessage = 'Uploading.... please wait...';
    modal.close();
    this.service.updateAnnouncement(this.announcementForm.value).subscribe(() => {
      this.service.getAllAnnouncements().subscribe(announcements => this.announcements = announcements);
    });
  }

  activateAnnouncement(id: number, timeout: number) {
    this.service.activateAnnouncement(id, timeout * 60).subscribe();
  }
}
