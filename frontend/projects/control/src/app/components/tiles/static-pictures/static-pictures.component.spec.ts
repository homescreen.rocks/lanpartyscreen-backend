import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { StaticPicturesComponent } from './static-pictures.component';

describe('StaticPicturesComponent', () => {
  let component: StaticPicturesComponent;
  let fixture: ComponentFixture<StaticPicturesComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ StaticPicturesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StaticPicturesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
