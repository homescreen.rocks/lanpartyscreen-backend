import {Component, OnInit} from '@angular/core';
import {StaticPicturesService} from '../../../services/static-pictures.service';
import {Picture} from '../../../classes/picture';
import {faFileUpload} from '@fortawesome/free-solid-svg-icons';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-static-pictures',
  templateUrl: './static-pictures.component.html',
  styleUrls: ['./static-pictures.component.scss'],
})
export class StaticPicturesComponent implements OnInit {

  pictures: Array<Picture> = [];
  faUpload = faFileUpload;
  selectedPic: Picture;
  currentTitle: string;
  currentShowOnBeamerState: boolean;
  currentTimeout = 10;
  currentFile;
  uploadingMessage;

  constructor(private service: StaticPicturesService, private modalService: NgbModal) {
  }

  ngOnInit() {
    this.service.getAllPictures().subscribe(pics => this.pictures = pics);
  }

  public openUploadModal(modal) {
    this.currentShowOnBeamerState = true;
    this.modalService.open(modal).result.then(() => {
      this.service.getAllPictures().subscribe(pics => this.pictures = pics);
    }, () => {
    });
  }

  public openModal(modal, picture: Picture) {
    this.selectedPic = picture;
    this.modalService.open(modal).result.then(() => {
      this.service.getAllPictures().subscribe(pics => this.pictures = pics);
    }, () => {
    });
  }

  deletePicture(selectedPic: Picture, modal) {
    this.service.deletePicture(selectedPic.id).subscribe(() => modal.close('closed after deletion'));
  }

  updateUploadFile($event) {
    const files = $event.target.files;
    if (!files.length) {
      delete this.currentFile;
    }
    const reader = new FileReader();
    reader.onload = (event: any) => {
      this.currentFile = event.target.result;
    };
    reader.readAsDataURL(files[0]);
  }

  uploadPicture(modal) {
    this.uploadingMessage = 'Uploading.... please wait...';
    this.service.addPicture(this.currentFile, this.currentTitle, this.currentShowOnBeamerState).subscribe(() => {
      this.uploadingMessage = null;
      this.currentTitle = null;
      this.currentFile = null;
      modal.close('closed after upload');
    });
  }

  activatePicture(id: number, timeout: number) {
    this.service.activatePicture(id, timeout * 60).subscribe();
  }

  handleChangeShowOnBeamer(id: number, $event: Event) {
    const newState = ($event.target as HTMLInputElement).checked;
    if (newState === true) {
      this.service.enablePictureForStream(id).subscribe();
    } else {
      this.service.disablePictureForStream(id).subscribe();
    }
  }
}
