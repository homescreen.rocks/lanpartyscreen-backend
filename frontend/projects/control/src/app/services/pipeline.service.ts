import {Component, Injectable, Input} from '@angular/core';
import {PipelineEntry} from '../classes/pipeline-entry';
import {BehaviorSubject, Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {NgbActiveModal, NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {HubConnection, HubConnectionBuilder} from '@microsoft/signalr';

@Injectable({
  providedIn: 'root'
})
export class PipelineService {
  private connection: HubConnection;
  private _activeModule: BehaviorSubject<string> = new BehaviorSubject<string>(null);
  private _autoplayOn: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(null);

  constructor(private http: HttpClient, private modalService: NgbModal) {
    this.connection = new HubConnectionBuilder()
      .withUrl('/pipeline')
      .build();

    this.connection.onclose(err => {
      const modalRef = this.modalService.open(SignalrErrorModalComponent, {backdrop: 'static'});
      modalRef.componentInstance.error = err;
    });

    this.connection.start().catch(err => {
      const modalRef = this.modalService.open(SignalrErrorModalComponent, {backdrop: 'static'});
      modalRef.componentInstance.error = err;
    });

    this.connection.on('publish', (entry: PipelineEntry) => {
      console.log(entry);
      if (entry === null) {
        console.warn('Modul vom typ null gibt es nicht!');
      } else {
        this._activeModule.next(entry.type);
      }
    });

    this.connection.on('autoplay', (state: string) => {
      this._autoplayOn.next(state === 'on');
    });
  }

  get activeModule(): Observable<string> {
    return this._activeModule.asObservable();
  }

  get autoPlayState(): Observable<boolean> {
    return this._autoplayOn.asObservable();
  }

  setAutoPlayState(state: boolean) {
    this.http.post('/api/v1/Pipeline/' + (state ? 'run' : 'stop'), null).subscribe();
  }

  skip() {
    this.http.post('/api/v1/Pipeline/skip', null).subscribe();
  }
}

@Component({
  selector: 'app-signalr-error-modal',
  template: `
    <div class="modal-header">
      <h4 class="modal-title">Error!</h4>
    </div>
    <div class="modal-body">
      <p>Websocket connection was lost!</p>
      <p>{{error}}</p>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-primary" (click)="reload()">Refresh page</button>
    </div>
  `
})
export class SignalrErrorModalComponent {
  @Input() error;

  constructor(public activeModal: NgbActiveModal) {
  }

  public reload() {
    window.location.reload();
  }
}
