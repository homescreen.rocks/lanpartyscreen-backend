import {animate, style, transition, trigger} from '@angular/animations';
import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  animations: [
    trigger('container', [
      transition(':enter', [
        style({opacity: '0'}),
        animate('1000ms ease-out'), style({opacity: '1'})
      ]),
    ])
  ]
})
export class HeaderComponent implements OnInit {

  @Input() main: string;
  @Input() small: string;


  constructor() {
  }

  ngOnInit() {
  }

}
