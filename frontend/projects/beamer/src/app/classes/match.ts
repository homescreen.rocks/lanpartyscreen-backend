import {Team} from './team';

export class Match {
  id: number;
  teams: Team[];
}
