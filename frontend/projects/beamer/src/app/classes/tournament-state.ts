export enum TournamentState {
  Inactive = 'Inactive',
  Joining = 'Joining',
  Running = 'Running',
  Finished = 'Finished'
}
