export class WelcomeMessage {
  nickName: string;
  checkInTime: Date;
  lastName: string;
  firstName: string;
}
