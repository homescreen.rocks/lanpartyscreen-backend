import {PipelineEntryNews} from './pipeline-entry-news';
import {PipelineEntryShout} from './pipeline-entry-shout';
import {PipelineEntryPicture} from './pipeline-entry-picture';
import {PipelineEntryMatchResult} from './pipeline-entry-match-result';
import {PipelineEntryAnnouncement} from './pipeline-entry-announcement';
import {PipelineEntryWelcomeMessage} from './pipeline-entry-welcome-message';
import {PipelineEntryTournament} from './pipeline-entry-tournament';

export type PipelineEntry =
  PipelineEntryNews
  | PipelineEntryShout
  | PipelineEntryPicture
  | PipelineEntryMatchResult
  | PipelineEntryAnnouncement
  | PipelineEntryWelcomeMessage
  | PipelineEntryTournament;
