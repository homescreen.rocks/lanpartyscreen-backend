export class Announcement {
  id: number;
  headline: string;
  text: string;
  iconClass: string;
  postedAt: Date;
  updatedAt: Date;
}
