export enum ModuleType {
  News = 'News',
  Announcements = 'Announcements',
  Shouts = 'Shouts',
  Tournaments = 'Tournaments',
  Matches = 'Matches',
  MatchResults = 'MatchResults',
  MatchResult = 'MatchResult',
  Pictures = 'Pictures',
  Timeline = 'Timeline',
  WelcomeMessage = 'WelcomeMessage',
}
