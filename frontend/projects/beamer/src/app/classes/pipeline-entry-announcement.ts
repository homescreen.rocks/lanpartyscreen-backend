import {PipelineEntryAbstract} from './pipeline-entry-abstract';
import {ModuleType} from './moduleType';
import {Announcement} from './announcement';

export class PipelineEntryAnnouncement extends PipelineEntryAbstract {
  type = ModuleType.Announcements;
  payload: Announcement;
}
