import {CommonModule, registerLocaleData} from '@angular/common';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import localeDe from '@angular/common/locales/de';
import {LOCALE_ID, NgModule} from '@angular/core';
import {MatButtonModule} from '@angular/material/button';
import {MatCardModule} from '@angular/material/card';
import {MatDialogModule} from '@angular/material/dialog';
import {MatDividerModule} from '@angular/material/divider';
import {MatGridListModule} from '@angular/material/grid-list';
import {MatTableModule} from '@angular/material/table';
import {MatTabsModule} from '@angular/material/tabs';
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {VerticalTimelineModule} from 'angular-vertical-timeline';

import {AppComponent} from './app.component';
import {MainComponentComponent} from './main-component/main-component.component';
import {SignalrErrorModalComponent} from './services/pipeline.service';
import {HeaderComponent} from './shared-components/header/header.component';
import {AnnouncementsComponent} from './slides/announcements/announcements.component';
import {FinishedMatchesComponent} from './slides/finished-matches/finished-matches.component';
import {ConfettiCanvasComponent} from './slides/match-result/confetti-canvas/confetti-canvas.component';
import {MatchResultComponent} from './slides/match-result/match-result.component';
import {NewsComponent} from './slides/news/news.component';
import {PicturesComponent} from './slides/pictures/pictures.component';
import {ShoutsComponent} from './slides/shouts/shouts.component';
import {TimelineComponent} from './slides/timeline/timeline.component';
import {TournamentsComponent} from './slides/tournaments/tournaments.component';
import {UpcomingMatchesComponent} from './slides/upcoming-matches/upcoming-matches.component';
import {WelcomeMessageComponent} from './slides/welcome-message/welcome-message.component';
import {MatchRowComponent} from './shared-components/match-row/match-row.component';
import {AnimateInContainerComponent} from './shared-components/animate-in-container/animate-in-container.component';
import {TournamentRowComponent} from './shared-components/tournament-row/tournament-row.component';
import {BackgroundComponent} from './shared-components/background/background.component';
import {MarkdownModule} from 'ngx-markdown';

registerLocaleData(localeDe);

@NgModule({
  declarations: [
    AppComponent,
    SignalrErrorModalComponent,
    MainComponentComponent,
    NewsComponent,
    ShoutsComponent,
    PicturesComponent,
    WelcomeMessageComponent,
    AnnouncementsComponent,
    MatchResultComponent,
    TournamentsComponent,
    TimelineComponent,
    ConfettiCanvasComponent,
    UpcomingMatchesComponent,
    FinishedMatchesComponent,
    HeaderComponent,
    MatchRowComponent,
    AnimateInContainerComponent,
    TournamentRowComponent,
    BackgroundComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    BrowserAnimationsModule,
    CommonModule,
    MatTabsModule,
    MatButtonModule,
    MatDialogModule,
    MatCardModule,
    MatTableModule,
    MatGridListModule,
    VerticalTimelineModule,
    FontAwesomeModule,
    MatDividerModule,
    MarkdownModule.forRoot({loader: HttpClient}),
  ],
  providers: [{provide: LOCALE_ID, useValue: 'de-DE'}],
  bootstrap: [AppComponent],
})
export class AppModule {
}
